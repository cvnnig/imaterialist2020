import random
import cv2

from detectron2.utils.visualizer import Visualizer

if __name__=="__main__":
    import detectron2
    # Source: https://www.dlology.com/blog/how-to-train-detectron2-with-custom-coco-datasets/
    from detectron2.data.datasets import register_coco_instances
    register_coco_instances("fruits_nuts", {}, "./data/trainval.json", "./data/images")

    fruits_nuts_metadata = detectron2.data.MetadataCatalog.get("fruits_nuts")
    print(fruits_nuts_metadata)

    dataset_dicts = detectron2.data.DatasetCatalog.get("fruits_nuts")
    print(fruits_nuts_metadata)

    # Randomly choose 3.
    for d in random.sample(dataset_dicts, 3):
        img = cv2.imread(d["file_name"])
        visualizer = Visualizer(img[:, :, ::-1], metadata=fruits_nuts_metadata, scale=0.5)
        vis = visualizer.draw_dataset_dict(d)
        cv2_imshow(vis.get_image()[:, :, ::-1])