import json
import os
import cv2
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pathlib import Path

# import some common detectron2 utilities
from detectron2.structures import BoxMode
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog, build_detection_train_loader, build_detection_test_loader
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg

from detectron2.utils.visualizer import ColorMode
from iMaterialist2020.imaterialist.data.datasets.rle_utils import KaggleRLE_to_mask, KaggleRLE_to_CocoBoundBoxes
from iMaterialist2020.imaterialist.data.dataset_mapper import DatasetMapper

from environs import Env

env = Env()
env.read_env()

# Get training dataframe
path_data = Path(env("path_raw"))
path_image = path_data / "train/"
path_output = Path(env("path_output"))

def get_training_images(path_images: Path = path_image):
    """
    Get the list of image fiels.
    :param path_images: 
    :return: 
    """
    images_list = []
    for dirname, _, filenames in os.walk(path_images):
        for filename in filenames:
            images_list.append(os.path.join(dirname, filename))
    return images_list


def load_dataset_into_dataframes(path_data: Path = path_data):
    """
    Get all the CSV from the competition into dataframes.
    """

    path_label = path_data / 'train.csv'
    df = pd.read_csv(path_label)

    # Get label descriptions
    with open(path_data/'label_descriptions.json', 'r') as file:
        label_desc = json.load(file)

    df_categories = pd.DataFrame(label_desc['categories'])
    df_attributes = pd.DataFrame(label_desc['attributes'])

    return df, df_attributes, df_categories

# Extremely ugly function - need to refactor
def attr_str_to_list(df):
    '''
    Function that transforms DataFrame AttributeIds which are of type string into a 
    list of integers. Strings must be converted because they cannot be transformed into Tensors
    '''
    # cycle through all the non NaN rows - NaN causes an error
    for index, row in df.iterrows():
        
        # Treating str differently than int
        if isinstance(row['AttributesIds'], str):
            
            # Convert each row's string into a list of strings             
            df['AttributesIds'][index] = row['AttributesIds'].split(',')
            
            # Convert each string in the list to int
            df['AttributesIds'][index] = [int(x) for x in df['AttributesIds'][index]]
            
        # If int - make it a list of length 1
        if isinstance(row['AttributesIds'], int):
            df['AttributesIds'][index] = [999]
            
        # Convert list to array
        df['AttributesIds'][index] = np.array(df['AttributesIds'][index])

        # Pad array with 0's so that all arrays are the same length - This will allows us to convert to tensor
        df['AttributesIds'][index] = np.pad(df['AttributesIds'][index], (0, 14-len(df['AttributesIds'][index]))) 
        

def create_datadict(df_labels_masks):
    """
    Creates the data dictionary necessary for Detectron2 which incorporated the additional following information:
        ImageId
        x0
        y0
        x1
        y1
    """

    # Get image file path required for dict and add it to our data frame

    # Get only the first 50K labels, out of 333K labels.
    datedic_labels_masks = df_labels_masks.copy()  # df sample

    # Append ImageId information.
    datedic_labels_masks['ImageId'] = str(path_image) + "/" + datedic_labels_masks['ImageId'] + ".jpg"

    # Get bboxes for each mask with our helper function
    # Turn list into array for proper indexing
    bboxes = [KaggleRLE_to_CocoBoundBoxes(c.EncodedPixels, c.Height, c.Width) for n, c in datedic_labels_masks.iterrows()]
    bboxes_array = np.array(bboxes)
    # Add each x, y coordinate as a column
    datedic_labels_masks['x0'], datedic_labels_masks['y0'], datedic_labels_masks['x1'], datedic_labels_masks['y1'] = bboxes_array[:, 0], bboxes_array[:, 1], bboxes_array[:, 2], bboxes_array[:, 3]
    
    #Replace NaNs from AttributeIds by 999
    datedic_labels_masks = datedic_labels_masks.fillna(999)
    
    # Turn attributes from string to list of ints with padding
    attr_str_to_list(datedic_labels_masks) 
    
    return datedic_labels_masks

# https://detectron2.readthedocs.io/tutorials/datasets.html
# https://colab.research.google.com/drive/16jcaJoc6bCFAQ96jDe2HwtXj7BMD_-m5
def convert_to_datadict(df_input):
    """

    :param df_input:
    :return:
    """
    dataset_dicts = []

    # Find the unique list of imageId, we will build the
    list_unique_ImageIds = df_input['ImageId'].unique().tolist()
    for idx, filename in enumerate(list_unique_ImageIds):

        record = {}
        
        # Convert to int otherwise evaluation will throw an error
        record['height'] = int(df_input[df_input['ImageId'] == filename]['Height'].values[0])
        record['width'] = int(df_input[df_input['ImageId'] == filename]['Width'].values[0])
        
        record['file_name'] = filename
        record['image_id'] = idx

        objs = []
        for index, row in df_input[(df_input['ImageId'] == filename)].iterrows():

            # Get binary mask
            mask = KaggleRLE_to_mask(row['EncodedPixels'], row['Height'], row['Width'])

            # opencv 4.2+
            # Transform the mask from binary to polygon format
            contours, hierarchy = cv2.findContours((mask).astype(np.uint8), cv2.RETR_TREE,
                                                    cv2.CHAIN_APPROX_SIMPLE)
            
            # opencv 3.2
            # mask_new, contours, hierarchy = cv2.findContours((mask).astype(np.uint8), cv2.RETR_TREE,
            #                                            cv2.CHAIN_APPROX_SIMPLE)
            
            segmentation = []

            for contour in contours:
                contour = contour.flatten().tolist()
                # segmentation.append(contour)
                if len(contour) > 4:
                    segmentation.append(contour) 

                    # Data for each mask
            obj = {
                'bbox': [row['x0'], row['y0'], row['x1'], row['y1']],
                'bbox_mode': BoxMode.XYXY_ABS,
                'category_id': row['ClassId'],
                'attributes': row['AttributesIds'], # New key: attributes
                'segmentation': segmentation,
                'iscrowd': 0
            }
            objs.append(obj)
        record['annotations'] = objs
        dataset_dicts.append(record)
    return dataset_dicts

def register_datadict(datadict_input, label_dataset:str = "sample_fashion_train"):
    """
    Register the data type with the Catalog function from Detectron2 code base.
    fixme: currently hard coded as sample_fashion_train sample_fashion_test
    """
    _, _, df_categories = load_dataset_into_dataframes()
    # Register the train and test and set metadata

    DatasetCatalog.register(label_dataset, lambda d=datadict_input: convert_to_datadict(d))
    MetadataCatalog.get(label_dataset).set(thing_classes=list(df_categories.name))

def show_training_labels(fashion_dict, fashion_metadata):
    # View some images + masks from the dataset
    import random
    plt.figure(figsize=(10, 10))
    for d in random.sample(fashion_dict, 3):
        img = cv2.imread(d["file_name"])
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        visualizer = Visualizer(img[:, :, ::-1], fashion_metadata, scale=0.5)
        vis = visualizer.draw_dataset_dict(d)
        plt.imshow(vis.get_image()[:, :, ::-1])

def set_training_config(testset_train: str, testset_test: str):
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.DATASETS.TRAIN = (testset_train,)  # must be a tuple
    cfg.DATASETS.TEST = (testset_test,)
    cfg.DATALOADER.NUM_WORKERS = 1
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
        "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # Let training initialize from model zoo

    ##### Input #####
    # Set a smaller image size than default to avoid memory problems

    # Size of the smallest side of the image during training
    cfg.INPUT.MIN_SIZE_TRAIN = (400,)
    # Maximum size of the side of the image during training
    cfg.INPUT.MAX_SIZE_TRAIN = 600

    # Size of the smallest side of the image during testing. Set to zero to disable resize in testing.
    cfg.INPUT.MIN_SIZE_TEST = 400
    # Maximum size of the side of the image during testing
    cfg.INPUT.MAX_SIZE_TEST = 600
    cfg.OUTPUT_DIR = str(path_output)
    cfg.SOLVER.IMS_PER_BATCH = 2
    cfg.SOLVER.BASE_LR = 0.00025
    cfg.SOLVER.MAX_ITER = 100000
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 512  # default: 512
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 46  # 46 classes in iMaterialist
    cfg.MODEL.ROI_HEADS.NUM_ATTRIBUTES = 294
    return cfg


class FashionTrainer(DefaultTrainer):
    'A customized version of DefaultTrainer. We add a mapping to the dataloader'
    
    @classmethod
    def build_train_loader(cls, cfg):
        return build_detection_train_loader(cfg, mapper=DatasetMapper(cfg))
    
    @classmethod
    def build_test_loader(cls, cfg, dataset_name):
        return build_detection_test_loader(cfg, dataset_name, mapper=DatasetMapper(cfg))
    

if __name__ == "__main__":

    data_full, df_attributes, df_categories = load_dataset_into_dataframes()

    datadic_full = create_datadict(data_full)
    # Arbitrary split in training / testing dataframes
    datadic_train_266k = datadic_full[:266000].copy()
    datadic_val_66k = datadic_full[-66000:].copy()

    # View image?
    #fashion_dict = get_fashion_dict(df_copy[:50])

    # Save metadata object for visualizations


    register_datadict(datadic_train_266k, "sample_fashion_train")
    register_datadict(datadic_val_66k, "sample_fashion_test")

    #show_training_labels(fashion_dict, fashion_metadata)

    cfg = set_training_config("sample_fashion_train", "sample_fashion_test")

    # Create output folder to dump all ouputs (tensorboard logs, model weigts, etc.)
    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

    trainer = FashionTrainer(cfg)

    # load weights
    trainer.resume_or_load(resume=False)

    # train loop
    trainer.train()


