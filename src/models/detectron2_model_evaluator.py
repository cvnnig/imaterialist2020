import json
import os
import cv2
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pathlib import Path

# import some common detectron2 utilities
from detectron2.structures import BoxMode
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg

from detectron2.utils.visualizer import ColorMode

from detectron2_model_train import set_training_config, create_datadict, load_dataset_into_dataframes, register_datadict

from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.data import build_detection_test_loader


from environs import Env

env = Env()
env.read_env()

# Get training dataframe
path_data = Path(env("path_raw"))
path_image = path_data / "train/"
path_output = Path(env("path_output"))
path_eval = Path(env("path_eval"))


# Get CFG

# Get Trainer


if __name__=="__main__":

    data_full, df_attributes, df_categories = load_dataset_into_dataframes()

    datadic_full = create_datadict(data_full)
    # Arbitrary split in training / testing dataframes
    datadic_train_266k = datadic_full[:266721].copy()
    datadic_val_66k = datadic_full[-66680:].copy()


    register_datadict(datadic_train_266k, "sample_fashion_train")
    register_datadict(datadic_val_66k, "sample_fashion_test")

    #show_training_labels(fashion_dict, fashion_metadata)

    cfg = set_training_config("sample_fashion_train", "sample_fashion_test")

    # Create output folder to dump all ouputs (tensorboard logs, model weigts, etc.)
    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)

    trainer = DefaultTrainer(cfg)

    # load weights
    trainer.resume_or_load(resume=False)


    #  Evaluate performance using AP metric implemented in COCO API
    evaluator = COCOEvaluator("sample_fashion_test", cfg, False, output_dir=str(path_output))
    val_loader = build_detection_test_loader(cfg, "sample_fashion_test")
    inference_on_dataset(trainer.model, val_loader, evaluator)