# imports
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms

import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.onnx
import sys
import os
import math
from PIL import Image
import time

import zetane.gradcam as gc
import zetane.context as ztn
import zetane
# WARNING: At the moment, the self._run_checker attribute in onnx.py should be set to True for the script to work.

# We start by creating a basic PyTorch convnet


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 32, 5)
        self.fc1 = nn.Linear(32 * 4 * 4, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 32 * 4 * 4)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

# Function to produce GradCAM heatmap images for predicted classes


def run_gradcam(net, img, classes, zimg):
    temp_g = gc.Gradcam(net, img, classes)
    zimg.update(data=temp_g.image_list)


def evaluation(net, dataloader):
    total, correct = 0, 0

    # keeping the network in evaluation mode
    net.eval()
    for data in dataloader:
        inputs, labels = data
        # moving the inputs and labels to gpu
        inputs, labels = inputs.to(device), labels.to(device)
        outputs = net(inputs)
        _, pred = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (pred == labels).sum().item()
    return 100 * correct / total


def train(net, criterion, optimizer, epochs=5, visual_training=False):
    loss_arr = []
    loss_epoch_arr = []
    running_loss = 0.0

    # The training loop - this is where the fun begins...
    for epoch in range(epochs):
        total, correct = 0, 0
        ztxt_epoch.text("Epoch: " + str(epoch + 1)).update()

        for i, data in enumerate(trainloader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            target = labels.cpu().numpy()[0]
            pred = torch.argmax(F.softmax(outputs, dim=1)).cpu().numpy()
            total += labels.size(0)
            if (pred == target):
                correct += 1

            running_loss += loss.item()
            # let's display the whole pipeline every 100 mini-batches...
            if visual_training and i % 100 == 1:
                acc = (correct / total) * 100

                ztxt_target.text("Target class: " + classes[target] + "- " + str(target)).update()
                ztxt_pred.text("Predicted class: " + classes[pred] + "- " + str(pred)).update()
                ztxt_loss.text("Running Loss: " + str(running_loss/i)).update()
                ztxt_acc.text("Accuracy: " + str(acc)).update()
                ztxt_correct.text("Correct: " + str(correct)).update()
                ztxt_total.text("Total: " + str(total)).update()

                zonnx.update(model=net, inputs=inputs)
                run_gradcam(net, inputs, classes, zimg)


        running_loss = 0.0
        loss_epoch_arr.append(loss.item())
        print('Epoch: %d/%d, Test acc: %0.2f, Train acc: %0.2f' %
        (epoch+1, epochs, evaluation(net, testloader_batch), evaluation(net, trainloader_batch)))

    torch.save(net, 'yolov3_model.pt')


print('Finished Training')


def predict_in_zetane(net, criterion):
    running_loss = 0.0
    for i, data in enumerate(testloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)

        # set the model to eval mode
        net.eval()

        # get predictions
        outputs = net(inputs)
        loss = criterion(outputs, labels)

        target = labels.cpu().numpy()[0]
        pred = torch.argmax(F.softmax(outputs, dim=1)).cpu().numpy()
        total += labels.size(0)
        if (pred == target):
            correct += 1

        running_loss += loss.item()
        # let's display the whole pipeline every 100 mini-batches...
        if i % 100 == 1:
            acc = (correct / total) * 100

            ztxt_target.text("Target class: " + classes[target] + "- " + str(target)).update()
            ztxt_pred.text("Predicted class: " + classes[pred] + "- " + str(pred)).update()
            ztxt_loss.text("Running Loss: " + str(running_loss/i)).update()
            ztxt_acc.text("Accuracy: " + str(acc)).update()
            ztxt_correct.text("Correct: " + str(correct)).update()
            ztxt_total.text("Total: " + str(total)).update()

            zonnx.update(model=net, inputs=inputs)
            run_gradcam(net, inputs, classes, zimg)


def main(load_model=True):
    print("Building pytorch model")
    net = Net()
    net.to(device)
    # Initialize convnet, define loss and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
    train(net, epochs=5, criterion=criterion,
            optimizer=optimizer, visual_training=True)

    predict_in_zetane(net, criterion=criterion)


# transforms
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5,), (0.5,))])


# datasets
trainset = torchvision.datasets.FashionMNIST('data',
                                             download=True,
                                             train=True,
                                             transform=transform)
testset = torchvision.datasets.FashionMNIST('data',
                                            download=True,
                                            train=False,
                                            transform=transform)

# dataloaders
trainloader = torch.utils.data.DataLoader(trainset, batch_size=1,
                                          shuffle=True, num_workers=0)

testloader = torch.utils.data.DataLoader(testset, batch_size=1,
                                         shuffle=False, num_workers=0)

trainloader_batch = torch.utils.data.DataLoader(trainset, batch_size=128,
                                                shuffle=True, num_workers=0)

testloader_batch = torch.utils.data.DataLoader(testset, batch_size=128,
                                               shuffle=False, num_workers=0)

# Class names
classes = ('T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
           'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle Boot')

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('Using device:', device)

# launch Zetane engine
zcontext = ztn.Context()
zcontext.launch()

# Zetane image object for the input
zimg_input = zcontext.image()

# Zetane image object for GradCAM heatmaps
zimg = zcontext.image()

# Zetane ONNX object for the convnet
zonnx = zcontext.onnx()
zonnx.visualize_inputs(True)

# Zetane text objects to display target and predicted classes
ztxt_target = zcontext.text("Target class: ").position(7, -1, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_pred = zcontext.text("Predicted class: ").position(7, -1.5, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_loss = zcontext.text("Loss: ").position(7, -2.0, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_acc = zcontext.text("Accuracy: ").position(7, -2.5, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_correct = zcontext.text("Correct: ").position(7, -3.0, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_total = zcontext.text("Total: ").position(7, -3.5, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()
ztxt_epoch = zcontext.text("Epoch: ").position(7, -4.0, 1).font('slab').font_size(0.16).billboard(True) \
    .gradient(color_list=((0.1, 0.8, 0.3), (0.4, 0.8, 0.4), (0, 1, 1))).color((1, 0, 1)).highlight((0.5, 0.5, 1))\
    .update()

if __name__ == "__main__":
    main()
