FROM python:3.8-slim-buster

RUN apt-get update -y

# gcc compiler and opencv prerequisites
RUN apt-get -y install nano git build-essential libglib2.0-0 libsm6 libxext6 libxrender-dev


# Detectron2 prerequisites
RUN pip install torch==1.5.0+cu101 torchvision==0.6.0+cu101 -f https://download.pytorch.org/whl/torch_stable.html
RUN pip install cython
RUN pip install -U 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'

# Source: https://towardsdatascience.com/detectron2-the-basic-end-to-end-tutorial-5ac90e2f90e3

# Detectron2 - CPU copy
RUN python -m pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu101/index.html

# Development packages
RUN pip install flask flask-cors requests opencv-python

ADD ./test.py /detectron2/
#RUN ["python", "/detectron2/test.py"]
#ENTRYPOINT ["python", "/detectron2/test.py"]