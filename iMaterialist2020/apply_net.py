import pandas as pd
import logging
import cv2
import os
import glob
import pickle
from environs import Env
from pathlib import Path
from typing import Any, ClassVar, Dict, List

import matplotlib.pyplot as plt
import detectron2.utils.comm as comm
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor, default_argument_parser, default_setup, launch
from detectron2.utils.logger import setup_logger, read_image
from detectron2.structures.instances import Instances
from detectron2.utils.logger import setup_logger
from detectron2.utils.visualizer import Visualizer
from iMaterialist2020.imaterialist.data.datasets.coco import register_datadict, MetadataCatalog
from iMaterialist2020.imaterialist.config import add_imaterialist_config
from iMaterialist2020.imaterialist.modeling import build_model

from detectron2.utils.visualizer import ColorMode

import torch
import detectron2.data.transforms as T
from detectron2.checkpoint import DetectionCheckpointer

LOGGER_NAME = "apply_net"
logger = logging.getLogger(LOGGER_NAME)

env = Env()
env.read_env()

path_data_interim = Path(env("path_interim"))
path_test_data = Path(env("test_dir"))
path_eval = Path(env("path_eval"))

# Load modified df for Detectron2 dataset dict 
# df_detectron = pd.read_feather('/home/yang.ding/git/imaterialist2020/data_imaterialist/imaterialist_train_multihot_n=266721.feather')

class DefaultPredictor:
    def __init__(self, cfg):
        self.cfg = cfg.clone()  # cfg can be modified by model
        self.model = build_model(self.cfg)
        self.model.eval()
        # ???
        self.metadata = MetadataCatalog.get(cfg.DATASETS.TEST[0])

        checkpointer = DetectionCheckpointer(self.model)
        checkpointer.load(cfg.MODEL.WEIGHTS)

        self.transform_gen = T.ResizeShortestEdge(
            [cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST], cfg.INPUT.MAX_SIZE_TEST
        )

        self.input_format = cfg.INPUT.FORMAT
        assert self.input_format in ["RGB", "BGR"], self.input_format

    def __call__(self, original_image):
        """
        Args:
            original_image (np.ndarray): an image of shape (H, W, C) (in BGR order).
        Returns:
            predictions (dict):
                the output of the model for one image only.
                See :doc:`/tutorials/models` for details about the format.
        """
        with torch.no_grad():  # https://github.com/sphinx-doc/sphinx/issues/4258
            # Apply pre-processing to image.
            if self.input_format == "RGB":
                # whether the model expects BGR inputs or RGB
                original_image = original_image[:, :, ::-1]
            height, width = original_image.shape[:2]
            image = self.transform_gen.get_transform(original_image).apply_image(original_image)
            image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))

            inputs = {"image": image, "height": height, "width": width}
            predictions = self.model([inputs])[0]
            return predictions

def get_input_file_list(file_path: str):
    if os.path.isdir(file_path):

        # Create a list of image file paths
        file_list = [
            os.path.join(file_path, fname)
            for fname in os.listdir(file_path)
            if os.path.isfile(os.path.join(file_path, fname))
        ]
    elif os.path.isfile(file_path):
        file_list = [file_path]
    else:
        file_list = glob.glob(file_path)
    return file_list

def execute_on_outputs(entry: Dict[str, Any], outputs: Instances):
    image_fpath = entry["file_name"]
    logger.info(f"Processing {image_fpath}")
    result = {"file_name": image_fpath}
    
    result["scores"] = outputs.get("scores").cpu()
    result["pred_boxes_XYXY"] = outputs.get("pred_boxes").tensor.cpu()
    result["attr_scores"] = outputs.get("attr_scores").cpu()
    result["masks"] = outputs.get()
    return result

def postexecute(result: Dict[str, Any]):
    out_fname = result["out_fname"]
    out_dir = os.path.dirname(out_fname)
    if len(out_dir) > 0 and not os.path.exists(out_dir):
        os.makedirs(out_dir)
    with open(out_fname, "wb") as hFile:
        pickle.dump(result["results"], hFile)
        logger.info(f"Output saved to {out_fname}")



def setup(args):
    cfg = get_cfg()
    add_imaterialist_config(cfg)
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.merge_from_file(args.config_file)
    
    cfg.merge_from_list(args.opts)
    cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")

    cfg.freeze()
    default_setup(cfg, args)
    # Setup logger for "imaterialist" module
    setup_logger(output=cfg.OUTPUT_DIR, distributed_rank=comm.get_rank(), name="imaterialist")
    return cfg


def main(args):

    datadic_train = pd.read_feather(path_data_interim / 'imaterialist_train_multihot_n=266721.feather')
    datadic_val = pd.read_feather(path_data_interim / 'imterailist_val_multihot_n=66680.feather')
    
    register_datadict(datadic_train, "sample_fashion_train")
    register_datadict(datadic_val, "sample_fashion_test")

    cfg = setup(args)

    # class iMatPredictor(DefaultPredictor):
    #     def __init__(self, cfg):
    #         super().__init__(cfg)
    #         self.model = build_model(cfg)

    im = cv2.imread(datadic_val.ImageId[0])

    predictor = DefaultPredictor(cfg)
    file_list = get_input_file_list(path_test_data)

    if len(file_list) == 0:
        logger.warning(f"No input images for {path_test_data}")
        return
    for file_name in file_list:
        img = read_image(file_name, format="BGR") # predictor takes BGR format
        with torch.no_grad():
            outputs = predictor(img)["instances"]
            result = execute_on_outputs({"file_name": file_name, "image": img}, outputs)
    postexecute(result)
    print(result[0])



def show_predicted_image(datadic_test, predictor):
    """
    Show 3 predicted images from the Fashion Dict (make sure it is the test set!)
    :param dict_test:
    :return:
    """
    import random
    from datetime import datetime
    from iMaterialist2020.imaterialist.data.datasets.make_dataset import load_category_attributes
    seed = random.randint(0, 99999999)

    # Randomly Grab 9 samples, iterate through rows of them, convert to list of tuple.  :
    list_tuple = list(datadic_test.sample(n=50, random_state=seed).iterrows())
    _, list_datadic = zip(*list_tuple)

    # Get meta data
    fashion_metadata = MetadataCatalog.get("sample_fashion_test")


    for i, d in enumerate(list_datadic):
        time_stamp = datetime.now().isoformat().replace(":", "")

        im = cv2.imread(d["ImageId"])
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        # Run through predictor
        outputs = predictor(im)

        # Visualize
        v = Visualizer(im[:, :, ::-1],
                       metadata=fashion_metadata,
                       scale=0.8,
                       instance_mode=ColorMode.IMAGE_BW  # remove the colors of unsegmented pixels
                       )
        # Bring the data back to CPU before passing to Numpy to draw
        v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        plt.imshow(v.get_image()[:, :, ::-1])

        plt.imsave(f"{path_eval}/{time_stamp}.png", v.get_image()[:, :, ::-1])


def main2():
    # load dataframe
    # fixme: this number needs to update or dynamic
    datadic_train = pd.read_feather(path_data_interim / 'imaterialist_train_multihot_n=266721.feather')
    datadic_test = pd.read_feather(path_data_interim / 'imterailist_val_multihot_n=66680.feather')

    register_datadict(datadic_train, "sample_fashion_train")
    register_datadict(datadic_test, "sample_fashion_test")

    #cfg = setup(args)
    cfg = get_cfg()

    # Add Solver etc.
    add_imaterialist_config(cfg)

    # Merge from config file.
    config_file = "/home/yang.ding/git/imaterialist2020/iMaterialist2020/configs/DevYang.yaml"
    cfg.merge_from_file(config_file)

    # Read one image, try to run predict.
    im = cv2.imread(datadic_test.ImageId[0])

    predictor = DefaultPredictor(cfg)

    example_data = predictor(im)

    print(predictor(im))
    show_predicted_image(datadic_test, predictor)

if __name__ == '__main__':
    main2()
    # args = default_argument_parser().parse_args()
    # args.config_file = "/home/nasty/imaterialist2020/iMaterialist2020/configs/exp01.yaml"
    # print("Command Line Args:", args)
    # launch(
    #     main,
    #     args.num_gpus,
    #     num_machines=args.num_machines,
    #     machine_rank=args.machine_rank,
    #     dist_url=args.dist_url,
    #     args=(args,),
    # )







#  @classmethod
#     def setup_config(
#         cls: type, config_fpath: str, model_fpath: str, args: argparse.Namespace, opts: List[str]
#     ):
#         opts.append("MODEL.ROI_HEADS.SCORE_THRESH_TEST")
#         opts.append(str(args.min_score))
#         if args.nms_thresh is not None:
#             opts.append("MODEL.ROI_HEADS.NMS_THRESH_TEST")
#             opts.append(str(args.nms_thresh))
#         cfg = super(ShowAction, cls).setup_config(config_fpath, model_fpath, args, opts)
#         return cfg


# def main():
#     parser = create_argument_parser()
#     args = parser.parse_args()
#     verbosity = args.verbosity if hasattr(args, "verbosity") else None
#     global logger
#     logger = setup_logger(name=LOGGER_NAME)
#     logger.setLevel(verbosity_to_level(verbosity))
# #     args.func(args)


# if __name__ == "__main__":
#     main()