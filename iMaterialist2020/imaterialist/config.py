from detectron2.config import CfgNode as CN
from detectron2 import model_zoo

def add_imaterialist_config(cfg: CN):
    """
    Add config for imaterialist2 head
    """

    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
        "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")

    ##### Input #####
    # Set a smaller image size than default to avoid memory problems

    # Size of the smallest side of the image during training
    # _C.INPUT.MIN_SIZE_TRAIN = (400,)
    # # Maximum size of the side of the image during training
    # _C.INPUT.MAX_SIZE_TRAIN = 600

    # # Size of the smallest side of the image during testing. Set to zero to disable resize in testing.
    # _C.INPUT.MIN_SIZE_TEST = 400
    # # Maximum size of the side of the image during testing
    # _C.INPUT.MAX_SIZE_TEST = 600
    
    cfg.SOLVER.IMS_PER_BATCH = 2
    cfg.SOLVER.BASE_LR = 0.00025
    cfg.SOLVER.MAX_ITER = 50000
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 512  # default: 512
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 46  # 46 classes in iMaterialist
    cfg.MODEL.ROI_HEADS.NUM_ATTRIBUTES = 295
