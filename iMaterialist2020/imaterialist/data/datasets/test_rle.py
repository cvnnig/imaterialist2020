import numpy as np
from pycocotools import mask
import pytest
from rle_utils import KaggleRLE_to_CocoRLE, KaggleRLE_to_mask,KaggleRLE_to_mask1, mask_to_uncompressed_CocoRLE, mask_to_KaggleRLE,KaggleRLE_to_CocoBoundBoxes, KaggleRLE_to_bbox

# Adopted from example shown at: https://stackoverflow.com/questions/49494337/encode-numpy-array-using-uncompressed-rle-for-coco-dataset
# Which made me aware how annoying this whole thing is.
@pytest.fixture
def get_ground_truth_mask():
    ground_truth_binary_mask = np.array([[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                                         [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                                         [  0,   0,   0,   0,   0,   1,   1,   1,   0,   0],
                                         [  0,   0,   0,   0,   0,   1,   1,   1,   0,   0],
                                         [  0,   0,   0,   0,   0,   1,   1,   1,   0,   0],
                                         [  0,   0,   0,   0,   0,   1,   1,   1,   0,   0],
                                         [  1,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                                         [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
                                         [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0]], dtype=np.uint8)
    return ground_truth_binary_mask

@pytest.fixture
def get_ground_truth_KaggleRLE():
    # Manually encode into KaggleRLE
    KaggleRLE = list(map(str, [6, 1, 47, 4, 56, 4, 65, 4]))
    ground_truth_KaggleRLE = " ".join(KaggleRLE)
    return ground_truth_KaggleRLE

def test_cocoencoding(get_ground_truth_KaggleRLE, get_ground_truth_mask):
    cocoRLE = KaggleRLE_to_CocoRLE(get_ground_truth_KaggleRLE, *get_ground_truth_mask.shape)

    # Ground truth from Stackoverflow:
    uncompressedCocoRLE = {'counts': [6, 1, 40, 4, 5, 4, 5, 4, 21], 'size': [9, 10]}
    compressedCoCoRLE = mask.frPyObjects(uncompressedCocoRLE, uncompressedCocoRLE.get('size')[0], uncompressedCocoRLE.get('size')[1])

    # Ensure compressed version is the same.
    assert cocoRLE.items() == compressedCoCoRLE.items()

    # Ensure the binary converted from these are the same.
    assert np.ndarray.all(mask.decode(cocoRLE) ==mask.decode(compressedCoCoRLE))
    assert np.ndarray.all(mask.toBbox(cocoRLE) == mask.toBbox(cocoRLE))

def test_mask2CompressedCocoRLE(get_ground_truth_mask):
    print(mask.encode(np.asfortranarray(get_ground_truth_mask)))

def failed_test_mask2CompressedCocoRLE1(get_ground_truth_mask):
    print(mask.frPyObjects(get_ground_truth_mask, *get_ground_truth_mask.shape))

def test_mask2UncompressedCocoRLE(get_ground_truth_mask):
    print(mask_to_uncompressed_CocoRLE(get_ground_truth_mask))

def test_mask2KaggleRLE(get_ground_truth_mask):
    print(mask_to_KaggleRLE(get_ground_truth_mask))

def test_decoding_KaggleRLE(get_ground_truth_KaggleRLE, get_ground_truth_mask):
    print(KaggleRLE_to_mask(get_ground_truth_KaggleRLE, *get_ground_truth_mask.shape))

def test_bbs(get_ground_truth_KaggleRLE, get_ground_truth_mask):
    assert(
        np.all(
            KaggleRLE_to_CocoBoundBoxes(get_ground_truth_KaggleRLE, *get_ground_truth_mask.shape)
            ==
            KaggleRLE_to_bbox(get_ground_truth_KaggleRLE, get_ground_truth_mask.shape))
           )

def test_bbs(get_ground_truth_KaggleRLE, get_ground_truth_mask):


    print(np.array(KaggleRLE_to_CocoBoundBoxes(get_ground_truth_KaggleRLE, *get_ground_truth_mask.shape)).shape)

    print(np.array(KaggleRLE_to_bbox(get_ground_truth_KaggleRLE, get_ground_truth_mask.shape)).shape)
