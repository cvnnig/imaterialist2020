# iMaterialist 2020 in Detectron2

Author Julien Beaulieu

- Fine-grained segmentation + attribute categorization task for fashion and apparel.

You can run train_net.py to train the model and optionally specify a yaml config file with --config_file path/cfg.yaml. (I also found that it saves a copy of the config to the output, something we previously thought wasn't the case).


I created a make_dataset.py module that saves the preprocessed dataframe to .feather (which is faster than pickle) so that we're not doing those steps everytimes we're training. I have the full dataset saved as well as a tiny version of it for testing
config.py contains configs specific to imaterialist (some of the current configs should probably be moved to yaml config since we'll probably change them eventually)
coco.py is where we're creating the dataset_dict required for detectron2 as well as the register_dataset functions