
import pandas as pd


from pathlib import Path

from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg


from detectron2_model_train import register_datadict

from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.data import build_detection_test_loader
from iMaterialist2020.imaterialist.config import add_imaterialist_config

from environs import Env

env = Env()
env.read_env()

# Get training dataframe
path_data = Path(env("path_raw"))
path_image = path_data / "train/"
path_output = Path(env("path_output"))
path_eval = Path(env("path_eval"))
path_data_interim = Path(env("path_interim"))


# Get CFG

# Get Trainer


if __name__=="__main__":
    # load dataframe
    # fixme: this number needs to update or dynamic
    datadic_train = pd.read_feather(path_data_interim / 'imaterialist_train_multihot_n=266721.feather')
    datadic_test = pd.read_feather(path_data_interim / 'imterailist_val_multihot_n=66680.feather')

    register_datadict(datadic_train, "sample_fashion_train")
    register_datadict(datadic_test, "sample_fashion_test")

    # cfg = setup(args)
    cfg = get_cfg()

    # Add Solver etc.
    add_imaterialist_config(cfg)

    # Merge from config file.
    config_file = "/home/yang.ding/git/imaterialist2020/iMaterialist2020/configs/DevYang.yaml"
    cfg.merge_from_file(config_file)

    trainer = DefaultTrainer(cfg)

    # load weights
    trainer.resume_or_load(resume=False)


    #  Evaluate performance using AP metric implemented in COCO API
    evaluator = COCOEvaluator("sample_fashion_test", cfg, False, output_dir=str(path_output))
    val_loader = build_detection_test_loader(cfg, "sample_fashion_test")
    inference_on_dataset(trainer.model, val_loader, evaluator)